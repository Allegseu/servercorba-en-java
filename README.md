# Instructions

**Run the command lines from a terminal located in the project path**

_Example of path:_

```
C:\Users\alexd\Documents\NetBeansProjects\corba
```

1. _run this command to generate the entire corba module based on the .idl interface_

```
idlj -fall Echo.idl
```

2. _Make sure you have the classes in the client and server packages coded before running this command, as it will compile your code for execution._

```
javac corba/*.java client/*.java server/*.java
```

3. _Run this command to initialize the ORB service (you can change the port to one that is free)_

```
start orbd -ORBInitialPort 1080
```

4. _Run this command to run the server (you must put the same port you used in the previous step)_

```
java server/ServerAOM -ORBInitialPort 1080 -ORBInitialHost localhost
```

5. _Run this command to run the client (you must also put the same port)_

```
java client/ClientImpl -ORBInitialPort 1080 -ORBInitialHost localhost
```

6. _Ready, you already have your app running successfully!_

## Important

**This application example works with _jdk1.8.0_111_**
